<?php require_once('php/autoloader.php'); ?>

<!DOCTYPE html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>...</title>
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:400,300">
	<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" type="text/css" href="css/custom.css">

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script type="text/javascript" src="js/jquery.mCustomScrollbar.min.js"></script>

</head>
<body>

<div class="col-left">
	<?php include('navigation.php'); ?>

</div>

<div class="tabs-group">
	<div class="tabs-item">
		<h2><a href="http://tympanus.net/codrops/" target="_blank">Codrops &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/tympanus');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://24ways.org/" target="_blank">24 Ways &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/24ways');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

    <div class="tabs-item">
    	<h2><a href="http://css-tricks.com/" target="_blank">CSS Tricks &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/CssTricks');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://csswizardry.com/" target="_blank">CSS Wizardry &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/csswizardrycom');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://css-weekly.com/" target="_blank">CSS Weekly &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/CSS-Weekly');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://wptavern.com/" target="_blank">WP Tavern &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://wptavern.com/feed');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://demosthenes.info/" target="_blank">Demosthenes &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://demosthenes.info/feed.php');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://www.smashingapps.com/" target="_blank">Smashing Apps &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/SmashingApps');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://pineapple.io/" target="_blank">Pineapple.io &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://pineapple.io/resources.atom');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://www.jqueryrain.com/" target="_blank">Jquery Rain &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/Jqueryrain');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://www.catswhocode.com/" target="_blank">Cats Who Code &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/Catswhocode');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://ui-cloud.com/" target="_blank">UI Cloud &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/Uicloud');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://codegeekz.com/" target="_blank">Code Geekz &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/codegeekz');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://www.pencilscoop.com/" target="_blank">Pencil Scoop &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/PencilScoopDesignCreativeBlog');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://sixrevisions.com/" target="_blank">Six Revisions &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/SixRevisions');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://pepsized.com/" target="_blank">Pepsized &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/pepsized');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://designgeekz.com/" target="_blank">Design Geekz &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/DesignGeekz');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://www.designkindle.com/" target="_blank">Design Kindle &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/DesignKindle');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://www.webdesignerdepot.com/" target="_blank">Web Designers Depot &rsaquo;</a></h2>
		<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds2.feedburner.com/webdesignerdepot?format=html');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
	</div>

	<div class="tabs-item">
		<h2><a href="http://www.smashingmagazine.com/" target="_blank">Smashing Magazine &rsaquo;</a></h2>
    	<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://rss1.smashingmagazine.com/feed/');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
    </div>

    <div class="tabs-item">
    	<h2><a href="http://uxmovement.com/" target="_blank">UX Movement &rsaquo;</a></h2>
    	<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/uxmovement');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
    </div>

    <div class="tabs-item">
    	<h2><a href="http://www.ourtuts.com/" target="_blank">Our Tuts &rsaquo;</a></h2>
    	<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds.feedburner.com/ourtutsfeed');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
    </div>

    <div class="tabs-item">
    	<h2><a href="http://www.hongkiat.com/" target="_blank">Hongkiat &rsaquo;</a></h2>
    	<?php
			$feed = new SimplePie();
			$feed->set_feed_url('http://feeds2.feedburner.com/24thfloor');
			$feed->init();
			$feed->handle_content_type();
		?>
		<?php foreach ($feed->get_items(0, 50) as $item): ?>
			<a href="<?php echo $item->get_permalink(); ?>" target="_blank">
				<span class="date"><?php echo $item->get_date('M d'); ?></span>
				<span class="title"><?php echo $item->get_title(); ?></span>
			</a>
		<?php endforeach; ?>
    </div>
</div>

<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>

