
$(document).ready(function() {
	$(".col").delay(2000).fadeIn(900);
	$(".tabs-nav").tabs(".tabs-item", {
		effect: "fade",
		fadeInSpeed: 900,
	});
});

(function($){
	$(window).load(function(){
		$(".col-left").mCustomScrollbar({
			set_width: false,
			set_height: false,
			horizontalScroll: false,
			scrollInertia: 300,
			mouseWheel: true,
			mouseWheelPixels: "auto",
			autoDraggerLength: false,
			autoHideScrollbar: false,
			scrollButtons:{
				enable: false,
			},
			contentTouchScroll: true,
			theme: "light-thin" /*"light", "dark", "light-2", "dark-2", "light-thick", "dark-thick", "light-thin", "dark-thin"*/
		});
	});
})(jQuery);